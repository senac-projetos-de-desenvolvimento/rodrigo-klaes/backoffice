import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/user.css'
import validateMappingTownhouse from './validateMapping';
import useReactRouter from 'use-react-router'

export default function Townhouse({disabled = false, type}){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({})
  console.log(values);
  const [address, setAddress] = useState({})
  const [townhouses, setTownhouses] = useState([])
  useEffect(() => {
    if(id){
      get(id, setValues)
    }
  }, [id])

  useEffect(() => {
    if(!id){
      getTwonhouses(setTownhouses)
    }
  }, [])

  const renderAddressInputs = () => {
    if(!id){
      return (<Create fields={fieldsToAddress} buttonsActions={buttonsActions}/>)
    }
  }

  const fields = [
    {field: 'input', type: 'text', title: 'Nome do usuário', id: "name", name: "name", value: values.name || '', disabled, onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'email', title: 'Email', id: "email", name: "email", value: values.email || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'CPF', id: "cpf", name: "cpf", value: values.cpf || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Telefone', id: "phone", name: "phone", value: values.phone || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'select', title: 'Tipo', id: "role", name: "role", value: values.role || '', disabled,  placeholder: "Selecione o tipo", options: roleOptions(), onChange: (target) => onChange(target, values, setValues)},
  ]

  const fieldsToAddress = [
    {field: 'input', type: 'text', title: 'Nome do endereço', id: "name", placeholder: 'Casa, trabalho, etc ...', name: "name", value: address?.name || '', disabled,  onChange: (target) => onChange(target, address, setAddress)},
    {field: 'input', type: 'text', title: 'Complemento', id: "complement", name: "complement", value: address?.complement || '', disabled,  onChange: (target) => onChange(target, address, setAddress)},
    {field: 'select', title: 'Condominío', id: "townhouse", name: "townhouse", value: address?.townhouse_id || '', disabled,  placeholder: "Selecione o condominío", options: townhouses, onChange: (target) => onChange(target, address, setAddress)},
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Usuário', id: "button_criar", label: 'Criar Usuário', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => create(values, setValues, address, setAddress)},
    {field: 'button', type: 'button', title: 'Editar Usuário', id: "button_criar", label: 'Editar Usuário', className: type !== "edit" ? "hide" : "btn btn-primary", onClick: () => edit(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={[]}/>
      <br></br>
      {
        renderAddressInputs()      
      }
      <Create fields={[]} buttonsActions={buttonsActions}/>
    </div>
  )
}

const create = async (values = {}, setValues, address, setAddress) => {
  const isTrue = validateMappingTownhouse(values)
  
  if(!isTrue){
    return
  }

  const { data: { data }, status } = await api.post('/users', values)

  if(status !== 201){
    alert('Erro ao criar usuário', status);
    return
  }
  await createAddress(address, data.id)

  setValues({})
  setAddress({})
}

const createAddress = async (address, user_id) => {
  const { status } = await api.post('/address', {...address, user_id})
  if(status !== 201) {
    alert('Usuário criado, mas ocorreu erro ao vincular endereço', status);
  }
}

const edit = async (values = {}, setValues) => {
  const { id, signatures: ommit, townhouses, ...dataValues} = values
  console.log(dataValues);
  // const { data } = await api.put(`/users/${id}`, dataValues)

  // if(!data){
  //   console.log('Erro ao editar usuário', data);
  // }

  // setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const get = async (id, setValues) => {
  const { data } = await api.get(`/users/${id}`)
  setValues(data)
}

const getTwonhouses = async (setValues) => {
  const { data: { data } } = await api.get(`/townhouses?getAll=true`)
  setValues(data)
}

const getAddresses = async (setValues) => {
  const { data: { data } } = await api.get(`/addresses?getAll=true`)
  console.log();
  setValues(data)
}

const roleOptions = () => {
  return [
    {
     value: 'admin',
     name: 'Administrador',
    },
    {
     value: 'client',
     name: 'Cliente',
    },
  ]
}
