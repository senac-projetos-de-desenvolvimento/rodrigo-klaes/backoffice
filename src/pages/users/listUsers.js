import React, {useState, useEffect} from 'react'
import List from '../../components/list'
import api from '../../utils/axios';
import './css/user.css'
import useReactRouter from 'use-react-router'

const URL_API = process.env.REACT_APP_ENV === 'develop' ?  process.env.REACT_APP_API_URL_DEV : process.env.REACT_APP_API_URL

const tableTitles = [
  {
    name: 'name',
    title: 'Nome'
  },
  {
    name: 'email',
    title: 'Email'
  },
  {
    name: 'phone',
    title: 'Telefone'
  },
  {
    name: 'role',
    title: 'Tipo'
  },
  {
    name: 'is_checked',
    title: 'Verificado'
  },
]

export default function ListUsers(){
  const { history } = useReactRouter()

  const [values, setValues] = useState([])
  const [search, setSearch] = useState([])
  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 1,
    size: 0
  })

  useEffect(() => {
    get(setValues, setPagination, pagination.page)
  }, [pagination.page])
  
  const actions = {
    handleShow,
    handleEdit,
    handleDelete
  }
  
  const buttons = (id, actions, history) => {
    return (
      <td>
        <button onClick={() => history.push('/signature/list/' + id)} className="btn btn-dark user-list-button-margin"><i className="fa fa-file-contract" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleShow(id, history)} className="btn btn-info user-list-button-margin"><i className="fa fa-eye" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleEdit(id, history)} className="btn btn-success user-list-button-margin"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleDelete(id, setValues)} className="btn btn-danger user-list-button-margin"><i className="fa fa-trash" aria-hidden="true"></i></button>
      </td>
    )
  }

  return (
    <>
      <div className="form-group">
        <input           
          type="text" 
          id="search"
          name="search"
          placeholder="Buscar"
          onChange={(target) => onChange(target, setSearch)}
        />
        <button className="btn btn-info user-list-button-margin" onClick={() => get(setValues, search)}><i className="fa fa-search" aria-hidden="true"></i></button>
        <button className="btn btn-success user-list-button-margin" onClick={() => history.push('/user/create')}><i className="fa fa-plus" aria-hidden="true"></i></button>
        <button className="btn btn-secondary user-list-button-margin" onClick={() => window.location.href = URL_API + '/users/export/all'}><i class="fa fa-file-excel"></i></button>
     </div>
      <List  titles={tableTitles} body={values} buttons={buttons} actions={actions} history={history} pagination={pagination} get={get} set={{setPagination, setValues: setValues}} search={search}/>
    </>
  )
}

const get = async (setValues, setPagination, page, search = '') => {
  const { data: { data, ...pageInfos } } = await api.get(`/users?search=${search}&page=${page}`)
  const regex = /^([0-9]{2})([0-9]{4,5})([0-9]{4})$/;
  const dataFormated = data.map(item => {
    var str = item.phone.replace(/[^0-9]/g, "").slice(0, 11);
    return {
      ...item,
      phone: str.replace(regex, "($1)$2-$3")
    }
  })

  setValues(dataFormated);
  setPagination(pageInfos);
}

const onChange = ({target: {value}}, setValues) => {
  setValues(value)
}

const handleShow = (id, history) => {
  history.push(`/user/show/${id}`)
}

const handleEdit = (id, history) => {
  history.push(`/user/edit/${id}`)
}

const handleDelete = async (id, setValues) => {
  const resposta = window.confirm('Confirma a exclusão?')
  if (resposta === false) {
    return
  }

  await api.delete(`/users/${id}`)
  get(setValues)
}
