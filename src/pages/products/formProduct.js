import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/product.css'
import validateMappingProduct from './validateMappingProduct';
import useReactRouter from 'use-react-router'

export default function Products({disabled = false, type}){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({})
  const [categories, setCategories] = useState([])

  useEffect(() => {
    getCategories(setCategories)
    if(id){
      getProduct(id, setValues)
    }
  }, [id])

  const fields = [
    {field: 'input', type: 'text', title: 'Nome do produto', id: "name", name: "name", value: values.name || '', disabled, onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Código', id: "code", name: "code", value: values.code || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'file', title: 'Foto do produto', id: "file", name: "file", disabled,  onChange: (target) => onChangeFile(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Preço', id: "price", name: "price", value: values.price || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'number', title: 'Quantidade', id: "amount", name: "amount", value: values.amount || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'select', title: 'Categoria', id: "category_id", name: "category_id", value: values.category_id || '', disabled,  placeholder: "Selecione a categoria", options: categoryOptions(categories), onChange: (target) => onChange(target, values, setValues)},
    {field: 'select', title: 'Unidade', id: "unity", name: "unity", value: values.unity || '', disabled,  placeholder: "Selecione a unidade", options: unityOptions(), onChange: (target) => onChange(target, values, setValues)},
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Produto', id: "button_criar", label: 'Criar Produto', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => createProduct(values, setValues)},
    {field: 'button', type: 'button', title: 'Editar Produto', id: "button_criar", label: 'Editar Produto', className: type !== "edit" ? "hide" : "btn btn-primary", onClick: () => editProduct(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={buttonsActions}/>
    </div>
  )
}

const createProduct = async (values = {}, setValues) => {
  const isTrue = validateMappingProduct(values)

  if(!isTrue){
    return
  }

  const formData = new FormData()

  formData.append('file', values.file)
  formData.append('name', values.name)
  formData.append('code', values.code)
  formData.append('price', (values.price).replace(',', '.'))
  formData.append('amount', values.amount)
  formData.append('unity', values.unity)
  formData.append('category_id', values.category_id)
  
  try{
    await api.post('/products/file', formData, {
      headers: {'Content-Type': 'multipart/form-data' }
    })

  } catch(err){
    console.log(err);
  }

  // if(!data){
  //   console.log('Erro ao criar produto', data);
  // }

  setValues({})
}

const editProduct = async (values = {}, setValues) => {
  const { id, ...dataValues} = values
  const { data: { data } } = await api.put(`/products/${id}`, dataValues)

  if(!data){
    console.log('Erro ao editar produto', data);
  }

  setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const onChangeFile= ({target: {name, files}}, values, setValues) => {
  setValues({...values, [name]: files[0]})
}

const getProduct = async (id, setValues) => {
  const { data } = await api.get(`/products/${id}`)
  setValues(data)
}

const getCategories = async (setCategories) => {
  const { data: { data } } = await api.get('/categories?getAll=true')
  setCategories(data)
}

const categoryOptions = (categories) => {
  return categories.map(category => {
    return {
      value: category.id,
      name: category.name
    }
  })
}

const unityOptions = () => {
  return [
    {
     value: 'litros',
     name: 'litros',
    },
    {
     value: 'mililitros',
     name: 'mililitros',
    },
    {
     value: 'Unidade',
     name: 'Unidade',
    },
    {
     value: 'quilogramas',
     name: 'quilogramas',
    },
    {
     value: 'gramas',
     name: 'gramas',
    },
  ]
}