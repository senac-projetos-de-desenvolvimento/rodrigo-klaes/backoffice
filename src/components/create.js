import React from 'react'
import fieldTypes from './fieldTypes'
export default function create({fields, buttonsActions, className = "product-create-box"}){
  return (
    <div className={className}>
      {
        fields.map((field, index) => input(field.field, field, index))
      }
      <div>
        { 
          buttonsActions.map((field, index) => input(field.field, field, index)) 
        }
      </div>
    </div>
  )
}

function input(fieldType, config, index){
  return fieldTypes(fieldType)(config, index)
}