import React from 'react'

export default function list({titles, body, buttons, actions, history, pagination: { totalPage, page = 1}, set: {setPagination, setValues}, get, search }) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          {
            titles.map(({title}, index) => (
              <th key={index}>{title}</th>
            ))
          }
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        
          {
            body.map((item, key) => {
              return (
              <tr key={key}>
                {
                  titles.map(({name}, index) => {
                    return (
                      <td key={index}>{item[name]}</td>
                    )
                  })
                }
                  {
                    buttons(item.id, actions, history)
                  }
              </tr>)
            })
          }
          <tr>
            <td colSpan={titles.length + 1}>
            <div aria-label="Navegação de página exemplo">
              <ul className="pagination d-flex justify-content-center">
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Anterior">
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Anterior</span>
                  </a>
                </li>
                {
                  
                  createPagesButtons(totalPage).map(pageNumber => {
                    return (
                    <li key={pageNumber} className={page == pageNumber ? "page-item active" : "page-item"} onClick={(event) => get(setValues, setPagination, event.target.name, search)}>
                      <a className="page-link" href="#" name={pageNumber}> 
                      {pageNumber}
                      </a>
                    </li>  
                  )})
                }
                
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Próximo">
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Próximo</span>
                  </a>
                </li>
              </ul>
            </div>
            </td>
          </tr>
      </tbody>
    </table>
  )
}

const createPagesButtons = (totalPage) => {
  const pages = []
  for (let index = 0; index < totalPage; index++) {
    pages[index] = index + 1
  }
  return pages
}

