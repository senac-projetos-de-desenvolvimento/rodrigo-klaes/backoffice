import React from 'react'

const fieldTypes = {
  input: ({type, title, id, name, placeholder = '', className = 'form-control', onClick, onChange, disabled = false, value = '', classNameDiv = 'form-group'}, index, accept = '') => {
    return (
      <div className={classNameDiv} key={index}>
        <label htmlFor={id}>{title}</label>
        <input           
          type={type} 
          className={className} 
          id={id} 
          name={name} 
          placeholder={placeholder}
          onChange={onChange}
          disabled={disabled}
          value={value}
          accept={accept}
        />
     </div>
    )
  },
  select: ({ title, id, name, placeholder = '', className = 'form-control', onClick, onChange, options, disabled = false, value = '', classNameDiv = 'form-group'}, index) => {
    
    return (
      <div className={classNameDiv} key={index}>
        <label htmlFor={id}>{title}</label>
        <select 
          className={className} 
          id={id} 
          name={name} 
          disabled={disabled}
          placeholder={placeholder} 
          onChange={onChange}
        >
          <option>{placeholder}</option>
          {
            options.map((option, key) => {
              return (<option selected={value === option.value ? true : false} key={key} value={option.value}>{option.name}</option>)
            })
          }
          
        </select>
      </div>
    )
  },
  button: ({type, className, onClick, label}, index) => {
    return (
      <button 
        key={index}
        type={type} 
        className={className} 
        onClick={onClick}
      >
        {label}
      </button>
    )
  }
}

export default (type) => {
  return fieldTypes[type]
}